package com.hackweek.battlexing.helpers

data class Resource<out T>(
    val status: Status,
    val code: Int?,
    val data: T?,
    val message: String?
) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun <T> success(code: Int, data: T): Resource<T> {
            return Resource(Status.SUCCESS, code, data, null)
        }

        fun <T> error(message: String?, code: Int?, data: T? = null): Resource<T> {
            return Resource(Status.ERROR, code, data, message)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(Status.LOADING, null, data, null)
        }
    }
}
