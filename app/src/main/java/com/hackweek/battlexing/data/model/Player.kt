package com.hackweek.battlexing.data.model

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Nonnull

class Player(@Nonnull @PrimaryKey val id: Int): Serializable {
    var name: String = ""

    @SerializedName("avatar_path")
    var avatarPath: String? = null
    var location: String = ""
    var password: String? = ""
}