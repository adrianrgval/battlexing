package com.hackweek.battlexing.data.remote.services

import com.hackweek.battlexing.data.model.Player
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface PlayerService {
    @GET("players")
    suspend fun getCurrentPlayers(): Response<List<Player>>

    @POST("players")
    suspend fun registerPlayer(@Body player: Player): Response<Void>
}