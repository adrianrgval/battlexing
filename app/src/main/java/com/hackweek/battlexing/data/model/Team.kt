package com.hackweek.battlexing.data.model

import androidx.room.PrimaryKey
import java.io.Serializable
import javax.annotation.Nonnull

data class Team(@Nonnull @PrimaryKey val id: Int) : Serializable {
    var name: String = ""
    var ships: ArrayList<ArrayList<String>> = arrayListOf()
    var bombs: ArrayList<String> = arrayListOf()
    var dashboard: ArrayList<Box> = arrayListOf()
}
