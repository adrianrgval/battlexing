package com.hackweek.battlexing.data.remote.services

import com.hackweek.battlexing.data.model.Bomb
import com.hackweek.battlexing.data.model.Game
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface GameService {
    @POST("games")
    suspend fun createGame(@Body game: Game): Response<Game>

    @GET("games/ongoing")
    suspend fun getOngoingGame(@Query("location") location: String): Response<Game>

    @POST("bombs")
    suspend fun postBomb(@Body bomb: Bomb): Response<Game>
}