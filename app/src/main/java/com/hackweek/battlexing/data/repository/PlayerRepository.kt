package com.hackweek.battlexing.data.repository

import com.hackweek.battlexing.data.model.Player
import com.hackweek.battlexing.data.remote.datasource.BaseDataSource
import com.hackweek.battlexing.data.remote.datasource.PlayerDataSource
import javax.inject.Inject

class PlayerRepository @Inject constructor(
    private val playerDataSource: PlayerDataSource
) : BaseDataSource() {

    suspend fun getCurrentPlayers() = playerDataSource.getCurrentPlayers()

    suspend fun registerPlayer(player: Player) = playerDataSource.registerPlayer(player)
}