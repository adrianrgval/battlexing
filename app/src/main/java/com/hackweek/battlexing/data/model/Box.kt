package com.hackweek.battlexing.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Box : Serializable {
    var coordinate = ""

    @SerializedName("is_bombed")
    var isBombed = false

    @SerializedName("is_ship")
    var isShip = false

    @SerializedName("is_end")
    var isEnd = false

    @SerializedName("is_start")
    var isStart = false

    @SerializedName("is_horizontal")
    var isHorizontal = false
}



