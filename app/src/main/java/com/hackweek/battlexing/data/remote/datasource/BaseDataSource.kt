package com.hackweek.battlexing.data.remote.datasource

import com.hackweek.battlexing.helpers.Constants.SERVER_TIMEOUT_CODE
import com.hackweek.battlexing.helpers.Resource
import retrofit2.Response

abstract class BaseDataSource {

    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {
        try {
            val response = call()
            val code = response.code()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return Resource.success(code, body)
            }
            return error(code, " ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return error(SERVER_TIMEOUT_CODE, e.message ?: e.toString())
        }
    }

    private fun <T> error(code: Int, message: String): Resource<T> {
        return Resource.error("Network call has failed for a following reason: $message", code)
    }
}
