package com.hackweek.battlexing.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Bomb : Serializable {
    var coordinate: String = ""

    @SerializedName("player_id")
    var playerId: String? = ""
}
