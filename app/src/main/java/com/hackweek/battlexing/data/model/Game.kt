package com.hackweek.battlexing.data.model

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Nonnull

data class Game(@Nonnull @PrimaryKey val id: Int) : Serializable {
    var location: String = ""

    @SerializedName("next_team")
    var nextTeam: Team = Team(0)

    @SerializedName("rival_team")
    var rivalTeam: Team = Team(0)

    @SerializedName("next_player")
    var nextPlayer: Player = Player(0)
    var winner: String? = null
}
