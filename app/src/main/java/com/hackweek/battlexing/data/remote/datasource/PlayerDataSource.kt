package com.hackweek.battlexing.data.remote.datasource

import com.hackweek.battlexing.data.model.Player
import com.hackweek.battlexing.data.remote.services.PlayerService
import javax.inject.Inject

class PlayerDataSource @Inject constructor(
    private val playerService: PlayerService
) : BaseDataSource() {

    suspend fun getCurrentPlayers() = getResult {
        playerService.getCurrentPlayers()
    }

    suspend fun registerPlayer(player: Player) = playerService.registerPlayer(player)
}