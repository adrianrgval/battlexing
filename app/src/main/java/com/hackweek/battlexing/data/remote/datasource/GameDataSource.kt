package com.hackweek.battlexing.data.remote.datasource

import com.hackweek.battlexing.data.model.Bomb
import com.hackweek.battlexing.data.model.Game
import com.hackweek.battlexing.data.model.Player
import com.hackweek.battlexing.data.remote.services.GameService
import com.hackweek.battlexing.data.remote.services.PlayerService
import javax.inject.Inject

class GameDataSource @Inject constructor(
    private val gameService: GameService
) : BaseDataSource() {

    suspend fun createGame(game: Game) = gameService.createGame(game)

    suspend fun getOngoingGame(location: String) = getResult {
        gameService.getOngoingGame(location)
    }

    suspend fun postBomb(bomb: Bomb) = getResult {
        gameService.postBomb(bomb)
    }
}