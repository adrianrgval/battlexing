package com.hackweek.battlexing.data.repository

import com.hackweek.battlexing.data.model.Bomb
import com.hackweek.battlexing.data.model.Game
import com.hackweek.battlexing.data.model.Player
import com.hackweek.battlexing.data.remote.datasource.BaseDataSource
import com.hackweek.battlexing.data.remote.datasource.GameDataSource
import com.hackweek.battlexing.data.remote.datasource.PlayerDataSource
import javax.inject.Inject

class GameRepository @Inject constructor(
    private val gameDataSource: GameDataSource
) : BaseDataSource() {

    suspend fun createGame(game: Game) = gameDataSource.createGame(game)

    suspend fun getOngoingGame(location: String) = gameDataSource.getOngoingGame(location)

    suspend fun postBomb(bomb: Bomb) = gameDataSource.postBomb(bomb)
}