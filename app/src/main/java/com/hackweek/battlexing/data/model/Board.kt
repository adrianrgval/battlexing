package com.hackweek.battlexing.data.model

import androidx.room.PrimaryKey
import java.io.Serializable
import javax.annotation.Nonnull

data class Board(@Nonnull @PrimaryKey val id: Int) : Serializable
