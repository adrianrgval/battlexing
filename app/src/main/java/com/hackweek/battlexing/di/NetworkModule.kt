package com.hackweek.battlexing.di

import com.google.gson.GsonBuilder
import com.hackweek.battlexing.BuildConfig
import com.hackweek.battlexing.data.remote.services.GameService
import com.hackweek.battlexing.data.remote.services.PlayerService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .client(
            OkHttpClient()
                .newBuilder()
//                .addInterceptor(
//                    LoggingInterceptor.Builder()
//                        .setLevel(Level.BODY)
//                        .request("Request")
//                        .response("Response")
//                        .addHeader("Accept", "application/json")
//                        .build()
//                )
                .build()
        )
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())) // Gson
        .build()

    @Provides
    fun providePlayerService(retrofit: Retrofit): PlayerService =
        retrofit.create(PlayerService::class.java)

    @Provides
    fun provideGameService(retrofit: Retrofit): GameService =
        retrofit.create(GameService::class.java)

}
