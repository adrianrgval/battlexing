package com.hackweek.battlexing.modules.board

import android.animation.Animator
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.hackweek.battlexing.R
import com.hackweek.battlexing.adapters.BoxAdapter
import com.hackweek.battlexing.data.model.Box
import com.hackweek.battlexing.databinding.ActivityBoardBinding
import androidx.recyclerview.widget.GridLayoutManager
import com.hackweek.battlexing.data.model.Game
import com.hackweek.battlexing.databinding.ItemBoxBinding
import dagger.hilt.android.AndroidEntryPoint
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import com.hackweek.battlexing.modules.main.MainActivity


@AndroidEntryPoint
class BoardActivity : AppCompatActivity() {

    lateinit var binding: ActivityBoardBinding
    lateinit var viewModel: BoardViewModel
    private lateinit var adapter: BoxAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_board)
        viewModel = ViewModelProvider(this)[BoardViewModel::class.java]

        binding.lifecycleOwner = this
        binding.activity = this
        binding.viewmodel = viewModel

        viewModel.currentGame.postValue(intent.getSerializableExtra("game") as Game?)

        Glide.with(this).asGif().load(R.drawable.background2).into(binding.boardImageBackground)

        initBoardAdapter()
        initObservers()
        viewModel.initBoxes()
    }

    private fun initObservers() {
        viewModel.animating.observe(this, {
            if (it == false) {
                viewModel.boxList.value?.let { it1 -> updateList(it1) }
            }
        })
        viewModel.boxList.observe(
            this,
            {
                if (viewModel.animating.value == false) {
                    updateList(it)
                }
            }
        )
        viewModel.currentGame.observe(this) {
            viewModel.currentPlayer.postValue(it.nextPlayer.id)
            viewModel.boxList.postValue(it.rivalTeam.dashboard)
        }
        viewModel.winner.observe(this, {
            playSound(R.raw.win)
            val builder: AlertDialog.Builder = AlertDialog.Builder(applicationContext)
            builder.setMessage("The team $it Won the game!")
            builder.setCancelable(false)
            builder.setPositiveButton(
                "Ok"
            ) { dialog, id ->
                startActivity(Intent(this, MainActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                finish()
            }

            val alertDialog: AlertDialog = builder.create()
            alertDialog.show()
        })
    }

    private fun initBoardAdapter() {
        adapter = BoxAdapter(
            object : BoxAdapter.BoxClickListener {
                override fun onClick(item: Box, binding: ItemBoxBinding) {
                    if (!item.isBombed && viewModel.animating.value == false && viewModel.loadingServices.value == false) {
                        viewModel.animating.postValue(true)
                        viewModel.loadingServices.postValue(true)
                        viewModel.postBomb(item.coordinate)
                        binding.boxImage.visibility = View.VISIBLE
                        binding.boxImage.addAnimatorListener(object : Animator.AnimatorListener {
                            override fun onAnimationStart(p0: Animator?) {
                                // TODO("Not yet implemented")
                            }

                            override fun onAnimationEnd(p0: Animator?) {
                                if (item.isShip) {
                                    Glide.with(binding.root.context).load(R.drawable.fire_static).into(binding.boxImage)
                                } else {
                                    Glide.with(binding.root.context).load(R.drawable.water_static).into(binding.boxImage)
                                }
                                viewModel.animating.postValue(false)
                            }

                            override fun onAnimationCancel(p0: Animator?) {
                                viewModel.animating.postValue(false)
                                // TODO("Not yet implemented")
                            }

                            override fun onAnimationRepeat(p0: Animator?) {
                            }
                        })
                        if (item.isShip) {
                            binding.boxImage.setAnimation(R.raw.fire)
                            binding.boxImage.playAnimation()
                            playSound(R.raw.bomb_ship)
                        } else {
                            binding.boxImage.setAnimation(R.raw.water)
                            binding.boxImage.playAnimation()
                            playSound(R.raw.bomb_water)
                        }
                    }
                }
            }
        )

        binding.recyclerBox.layoutManager = GridLayoutManager(this, 10)
        binding.recyclerBox.adapter = adapter
    }

    private fun playSound(resource: Int) {
        val mp: MediaPlayer = MediaPlayer.create(binding.root.context, resource)
        mp.start()
    }

    private fun updateList(list: List<Box>) {
        adapter.submitList(list)
    }

    override fun onBackPressed() {
    }
}
