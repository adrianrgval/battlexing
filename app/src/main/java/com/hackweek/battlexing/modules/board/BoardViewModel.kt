package com.hackweek.battlexing.modules.board

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hackweek.battlexing.data.model.Bomb
import com.hackweek.battlexing.data.model.Box
import com.hackweek.battlexing.data.model.Game
import com.hackweek.battlexing.data.repository.GameRepository
import com.hackweek.battlexing.helpers.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BoardViewModel @Inject constructor(
    private val gameRepository: GameRepository
) : ViewModel() {

    val boxList = MutableLiveData<List<Box>>()
    val currentGame = MutableLiveData<Game>()
    var currentPlayer = MutableLiveData<Int>()
    var animating = MutableLiveData<Boolean>()
    var loadingServices = MutableLiveData<Boolean>()
    var winner = MutableLiveData<String>()

    init {
        animating.postValue(false)
        loadingServices.postValue(false)
    }

    fun initBoxes() {
        val boxes = ArrayList<Box>()
        for (y in getYCoordinates()) {
            for (x in getXCoordinates()) {
                val box = Box()
                box.coordinate = y + x
                boxes.add(box)
            }
        }
        boxList.value = boxes
    }

    fun postBomb(coordinate: String) {
        val bomb = Bomb()
        bomb.coordinate = coordinate
        bomb.playerId = currentPlayer.value.toString()
        viewModelScope.launch {
            gameRepository.postBomb(bomb).let { response ->
                if (response.code == Constants.SERVER_SUCCESS_CODE) {
                    if (currentGame.value?.winner != null) {
                        winner.postValue(currentGame.value?.winner)
                    } else {
                        currentGame.postValue(response.data!!)
                    }
                }
                loadingServices.postValue(false)
            }
        }
    }

    fun checkMarkedBoxes() {
        val bombs = currentGame.value?.rivalTeam?.bombs
        bombs?.iterator()?.forEach { bomb ->
            val index = boxList.value?.find {
                bomb == it.coordinate
            }
            boxList.value!![boxList.value!!.indexOf(index)].isBombed = true
        }
    }

    fun checkShipBoxes() {
        val ships = currentGame.value?.rivalTeam?.ships
        ships?.iterator()?.forEach { ship ->
            ship.iterator().forEach { shipBox ->
                val index = boxList.value?.find {
                    shipBox == it.coordinate
                }
                boxList.value!![boxList.value!!.indexOf(index)].isShip = true
            }
        }
    }

    private fun getXCoordinates(): ArrayList<String> {
        return arrayListOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")
    }

    private fun getYCoordinates(): ArrayList<String> {
        return arrayListOf("A", "B", "C", "D", "E", "F", "G", "H", "I", "J")
    }
}
