package com.hackweek.battlexing.modules.main

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.hackweek.battlexing.R
import com.hackweek.battlexing.adapters.PlayerAdapter
import com.hackweek.battlexing.data.model.Game
import com.hackweek.battlexing.databinding.ActivityMainBinding
import com.hackweek.battlexing.modules.board.BoardActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel
    private lateinit var adapter: PlayerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]

        binding.lifecycleOwner = this
        binding.activity = this
        binding.viewmodel = viewModel

        initPlayerAdapter()
        initObservers()
    }

    private fun initObservers() {
        viewModel.players.observe(this) {
            adapter.submitList(it)
        }
        viewModel.error.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }
        viewModel.currentGame.observe(this) {
            openBoard(it)
        }
    }

    private fun initPlayerAdapter() {
        adapter = PlayerAdapter()
        binding.mainRecyclerPlayers.adapter = adapter
    }

    private fun openBoard(game: Game) {
        startActivity(Intent(this, BoardActivity::class.java).putExtra("game", game))
    }
}
