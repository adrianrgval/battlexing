package com.hackweek.battlexing.modules.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hackweek.battlexing.data.model.Game
import com.hackweek.battlexing.data.model.Player
import com.hackweek.battlexing.data.repository.GameRepository
import com.hackweek.battlexing.data.repository.PlayerRepository
import com.hackweek.battlexing.helpers.Constants
import com.hackweek.battlexing.helpers.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val playerRepository: PlayerRepository,
    private val gameRepository: GameRepository
) : ViewModel() {

    var players = MutableLiveData<List<Player>?>()
    var playerName = MutableLiveData("")
    var playerPassword = MutableLiveData("")
    var error = MutableLiveData<String>()
    var currentGame = MutableLiveData<Game>()

    init {
        getCurrentPlayers()
        getOngoingGame()
    }

    private fun getCurrentPlayers() {
        viewModelScope.launch(Dispatchers.IO) {
            players.postValue(playerRepository.getCurrentPlayers().data)
        }
    }

    fun registerPlayer() {
        val player = Player(0)
        player.name = playerName.value!!
        player.location = "vlc"
        player.avatarPath = null
        player.password = playerPassword.value!!
        viewModelScope.launch {
            playerRepository.registerPlayer(player).let { response ->
                if (response.isSuccessful) {
                    getCurrentPlayers()
                } else {
                    error.postValue("There was an error, please try again.")
                }
            }
        }
        clearAllFields()
    }

    fun createGame() {
        viewModelScope.launch {
            val newGame = Game(0)
            newGame.location = "vlc"
            gameRepository.createGame(newGame).let { response ->
                when (response.code()) {
                    Constants.SERVER_SUCCESS_CODE -> {
                        currentGame.postValue(response.body())
                    }
                    Constants.SERVER_BADREQUEST_CODE -> {
                        // TODO NO GAME PREPARED
                    }
                    else -> {
                        // TODO ERROR
                    }
                }
            }
        }
    }

    private fun getOngoingGame() {
        viewModelScope.launch {
            gameRepository.getOngoingGame("vlc").let { response ->
                if (response.status == Resource.Status.SUCCESS) {
                    currentGame.postValue(response.data!!)
                }
            }
        }
    }

    private fun clearAllFields() {
        playerName.postValue("")
        playerPassword.postValue("")
    }
}
