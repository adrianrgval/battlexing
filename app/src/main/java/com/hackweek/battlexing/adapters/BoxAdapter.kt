package com.hackweek.battlexing.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hackweek.battlexing.R
import com.hackweek.battlexing.data.model.Box
import com.hackweek.battlexing.databinding.ItemBoxBinding

class BoxAdapter(
    private val clickListener: BoxClickListener
) :
    ListAdapter<Box, BoxAdapter.ViewHolder>(ListAdapterCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var loading = false
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(private val binding: ItemBoxBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            item: Box,
            clickListener: BoxClickListener
        ) {
            binding.box = item
            binding.clickListener = clickListener

            if (item.isBombed) {
                binding.boxImage.visibility = View.VISIBLE
                if (item.isShip) {
                    Glide.with(binding.root.context).load(R.drawable.fire_static).into(binding.boxImage)
                    binding.boxText.text = ""
                } else {
                    Glide.with(binding.root.context).load(R.drawable.water_static).into(binding.boxImage)
                    binding.boxText.text = ""
                }
            } else {
                binding.boxImage.cancelAnimation()
                binding.boxImage.clearAnimation()
                binding.boxImage.visibility = View.GONE
                binding.boxText.text = item.coordinate
            }

            binding.root.setOnClickListener {
                clickListener.onClick(item, binding)
            }

            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemBoxBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class ListAdapterCallback : DiffUtil.ItemCallback<Box>() {
        override fun areItemsTheSame(oldItem: Box, newItem: Box): Boolean {
            return oldItem.coordinate == newItem.coordinate
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Box, newItem: Box): Boolean {
            return oldItem == newItem
        }
    }

    interface BoxClickListener {
        fun onClick(item: Box, binding: ItemBoxBinding)
    }
}
